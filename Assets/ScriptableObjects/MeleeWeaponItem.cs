﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LootItem/MeleeWeapon")]
[System.Serializable]
public class MeleeWeaponItem : WeaponItem {
    public float hitForce = 100f;
}
