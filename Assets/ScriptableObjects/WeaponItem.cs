﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class WeaponItem : InventoryItem {
    public string description;

    public int damage;

    public bool usesAmmo;
    public int bulletsPerMag;
    public int totalAmmo;

    public float timeBetweenShots;
    public float timeToReload;
    public float timeToDisplayEffects;
    public float timeToEquip;
    public float timeToUnEquip;

    public AudioClip shotSound;
    public AudioClip reloadSound;

    public Vector3 bulletSpawn;
    public Vector3 equipPosition;

    //public abstract void Initialize(GameObject obj);
    
}
