﻿using UnityEngine;

[CreateAssetMenu(menuName = "LootItem/Grenade")]
[System.Serializable]
public class GrenadeItem : ProjectileWeaponItem {
    public float timeToExplode = 5;
}
