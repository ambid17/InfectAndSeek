﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LootItem/ProjectileWeapon")]
[System.Serializable]
public class ProjectileWeaponItem : WeaponItem {
    public float projectileForce = 500;
    public GameObject projectile;
    public GameObject explosion;

    public float launchSpeed = 20;
    public float explosionRadius = 5;
    public float explosionPower = 5;
    public float explosionLift = 5;
}
