﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LootItem/RaycastWeapon")]
[System.Serializable]
public class RaycastWeaponItem : WeaponItem {
    public int range;
}
