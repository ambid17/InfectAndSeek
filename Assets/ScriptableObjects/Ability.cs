﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ability", menuName = "Ability")]
public abstract class Ability : ScriptableObject
{
    public string abilityName;
    public Sprite sprite;
    public AudioClip sound;
    public float coolDown;

    public abstract void Initialize(GameObject obj);
    public abstract void TriggerAbility();
}
