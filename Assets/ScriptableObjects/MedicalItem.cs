﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LootItem/MedicalItem")]
[System.Serializable]
public class MedicalItem : InventoryItem {
    public string description;

    public int healAmount;

    public float timeToHeal;
    public float timesToHeal;
    public float healInterval;
    public float timeToReload = 1;
    public float timeToEquip;
    public float timeToUnEquip;

    public AudioClip healSound;

    public Vector3 equipPosition;
}
