﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New LootItem", menuName = "Items")]
[System.Serializable]
public abstract class InventoryItem: ScriptableObject
{
    public string itemName = "New Item";
    public Sprite icon;
    public GameObject itemPrefab;
    public bool isIndestructible = false;
    public bool isStackable = false;
    public int stackAmount;
    public int maxStackAmount;
    public LootType lootType;
}
