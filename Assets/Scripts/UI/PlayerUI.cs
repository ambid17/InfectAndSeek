﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {
    [SerializeField]
    GameObject inGameUI;

    [SerializeField]
    GameObject pauseMenu;

    public Image crossHair;

	void Start () {
        PauseMenu.IsOn = false;
        pauseMenu.SetActive(false);
	}
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            TogglePauseMenu();
        }
    }

    public void TogglePauseMenu() {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        PauseMenu.IsOn = pauseMenu.activeSelf;

        inGameUI.SetActive(!PauseMenu.IsOn);

        if (PauseMenu.IsOn) {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        } else {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
