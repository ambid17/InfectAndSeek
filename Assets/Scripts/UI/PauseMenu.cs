﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class PauseMenu : MonoBehaviour {
    public static bool IsOn;

    private NetworkManager networkManager;

    [SerializeField]
    GameObject optionsMenu;

    [SerializeField]
    GameObject pauseMenuHome;

    private void Start() {
        networkManager = NetworkManager.singleton;
    }

    public void LeaveRoom() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        MatchInfo matchInfo = networkManager.matchInfo;
        networkManager.matchMaker.DropConnection(matchInfo.networkId, matchInfo.nodeId, 0, networkManager.OnDropConnection);
        //TODO add host migration
        networkManager.StopHost();
    }

    public void ToggleOptionsMenu() {
        pauseMenuHome.SetActive(!pauseMenuHome.activeSelf);
        optionsMenu.SetActive(!optionsMenu.activeSelf);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
