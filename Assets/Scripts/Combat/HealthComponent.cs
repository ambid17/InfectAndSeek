﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum TeamType
{
    Human,
    Infected,
    Object
}

public class HealthComponent : MonoBehaviour, IRespondsToDeath
{
    public int maxHealth;

    //[SyncVar]
    public int currentHealth;

    //[SyncVar]
    private bool _isDead;
    public bool isDead {
        get {
            return _isDead;
        }
        protected set {
            _isDead = value;
        }
    }

    [SerializeField]
    private Behaviour[] disableOnDeath;
    private bool[] wasEnabled;

    public TeamType teamType;
    //Array of components that need to do something when the object dies
    protected List<IRespondsToDeath> deathResponders = new List<IRespondsToDeath>();

    void Awake()
    {
        SetDefaults();
    }

    public void Setup() {
        wasEnabled = new bool[disableOnDeath.Length];
        for (int i = 0; i < wasEnabled.Length; i++) {
            wasEnabled[i] = disableOnDeath[i].enabled;
        }

        SetDefaults();
    }

    //Add deathResponder to collection of responders to notify upon death
    public void RegisterDeathResponder(IRespondsToDeath deathResponder)
    {
        deathResponders.Add(deathResponder);
    }

    public void RegisterTeam(TeamType teamType)
    {
        this.teamType = teamType;
    }

    public void SetDefaults()
    {
        currentHealth = maxHealth;
        _isDead = false;

        for (int i = 0; i < disableOnDeath.Length; i++) {
            disableOnDeath[i].enabled = wasEnabled[i];
        }

        //TODO turn on all colliders
        Collider _col = GetComponent<Collider>();
        if(_col != null) {
            _col.enabled = true;
        }
    }

    public virtual bool CanTakeDamage(DamageContext context)
    {
        if(!isDead)
        {
            return true;
            //TODO remove when teams are set
            HealthComponent enemyHealth = context.source.GetComponent<HealthComponent>();
            if(enemyHealth != null){
                TeamType sourceTeam = enemyHealth.teamType;
                return sourceTeam != teamType;
            }
        }
        return false;
    }
    public bool CanHeal() {
       return currentHealth < maxHealth ?  true : false;
    }

    public void Heal(int healAmount) {
        Debug.Log("Healing: " + healAmount);
        int newHealth = currentHealth + healAmount;
        if(newHealth > maxHealth) {
            newHealth = maxHealth;
        }

        currentHealth = newHealth;
    }

    public bool IsDead()
    {
        return _isDead;
    }

    //[ClientRpc]
    public virtual void RpcTakeDamage(DamageContext context)
    {
        Debug.Log("rpc take dmg ");
        if(CanTakeDamage(context))
        {
            currentHealth -= context.amount;
            Debug.Log(gameObject + " Took " + context.amount + " damage from " + context.source);
            if (currentHealth <= 0)
            {
                OnDeath(context);
            }
        }
    }

    public virtual void OnDeath(DamageContext context)
    {
        isDead = true;

        for (int i = 0; i < disableOnDeath.Length; i++) {
            disableOnDeath[i].enabled = false;
        }

        //TODO turn off all colliders
        Collider _col = GetComponent<Collider>();
        if (_col != null) {
            _col.enabled = false;
        }

        Debug.Log(transform.name + " is dead");

        StartCoroutine(Respawn());
        //foreach(IRespondsToDeath responder in deathResponders)
        //{
        //    responder.OnDeath(context);
        //}
    }

    private IEnumerator Respawn() {
        yield return new WaitForSeconds(GameManager.instance.matchSettings.respawnTime);

        SetDefaults();
        Transform _spawnPoint = NetworkManager.singleton.GetStartPosition();
        transform.position = _spawnPoint.position;
        transform.rotation = _spawnPoint.rotation;

        Debug.Log(transform.name + " respawned");
    }
}
