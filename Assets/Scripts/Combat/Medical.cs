﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum MedicalState {
    Unequipped, Equipping, UnEquipping, Healing,
    Reloading, Idle
}

[Serializable]
public class Medical : MonoBehaviour, Equipment {
    float healTimer;
    float healOverTimeTimer;
    float reloadTimer;
    float equipTimer;
    [SerializeField]
    MedicalItem item;

    public MedicalItem Item {
        get {
            return item;
        }
        set {
            item = value;
        }
    }

    PlayerObjectInteract playerObjectInteract;
    HealthComponent playerHealth; 
    Transform itemHolder;

    AudioSource healAudio;

    MedicalState state;

    public MedicalState medicalState {
        get {
            return state;
        }
        set {
            Debug.Log("medicalState.Set: " + value);
            state = value;
        }
    }

    void Start () {
        healTimer = 0;
        healOverTimeTimer = 0;
        reloadTimer = 0;
        equipTimer = 0;
    }

    void FixedUpdate() {
        if (Input.GetButton("Fire1")) {
            Activate();
        }
    }

    void Update () {
        healTimer += Time.deltaTime;
        healOverTimeTimer += Time.deltaTime;
        reloadTimer += Time.deltaTime;
        equipTimer += Time.deltaTime;
        if (medicalState == MedicalState.Reloading && reloadTimer >= Item.timeToReload) {
            StopReloading();
        }
        if (healTimer >= Item.timeToHeal && medicalState == MedicalState.Healing) {
            Deactivate();
        }
        if (equipTimer >= Item.timeToEquip && medicalState == MedicalState.Equipping) {
            medicalState = MedicalState.Idle;
        }
    }

    public void Activate() {
        if (CanActivate()) {
            medicalState = MedicalState.Healing;

            Item.stackAmount--;

            healTimer = 0;

            EnableEffects();
        }
    }

    IEnumerator ActivateHealOverTime() {
        for(int i = 0; i <= Item.timesToHeal; i++) {
            playerHealth.Heal(Item.healAmount);
            yield return new WaitForSeconds(Item.healInterval);
        }
    }

    public void AddToInventory() {
        //TODO check to make sure stack isn't at max in PlayerInventory
        if (itemHolder == null) {
            playerObjectInteract = GetComponentInParent<PlayerObjectInteract>();
            itemHolder = playerObjectInteract.inventory.gunHolder;

            playerHealth = playerObjectInteract.GetComponent<HealthComponent>();
        }

        SetAudio();

        SetTransform();

        gameObject.SetActive(false);
    }

    void SetAudio() {
        if (healAudio == null) {
            healAudio = gameObject.AddComponent<AudioSource>();
            healAudio.playOnAwake = false;
            healAudio.clip = Item.healSound;
        }
    }

    void SetTransform() {
        gameObject.transform.position = Vector3.zero;
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.eulerAngles = Vector3.zero;
        gameObject.transform.localEulerAngles = Vector3.zero;
    }

    public bool CanActivate() {
        if (medicalState == MedicalState.Idle 
            && item.stackAmount > 0
            && playerHealth.CanHeal()) {
            return true;
        } else {
            //TODO display something about being at max hp
            return false;
        }
    }

    public void Deactivate() {
        if (Item.timesToHeal > 0) {
            StartCoroutine(ActivateHealOverTime());
        } else {
            playerHealth.Heal(Item.healAmount);
        }

        if (medicalState != MedicalState.Idle) {
            medicalState = MedicalState.Idle;
            DisableEffects();
        }

        if(Item.stackAmount == 0) {
            GameObject.Destroy(gameObject);
        }
    }

    public void EnableEffects() {
        if (healAudio != null)
            healAudio.Play();
    }

    public void DisableEffects() {
        if (healAudio != null)
            healAudio.Stop();
    }

    public void Drop() {
        DisableEffects();

        gameObject.transform.parent = null;
        gameObject.AddComponent<BoxCollider>();

        Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.useGravity = true;
        rigidbody.AddForce(gameObject.transform.position * 2);
        rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        LootItem item = gameObject.AddComponent<LootItem>();
        item.item = this.item;

        gameObject.layer = 11;
        gameObject.SetActive(true);

        GameObject.Destroy(this);
    }

    public void StartEquipping() {
        equipTimer = 0;
        medicalState = MedicalState.Equipping;
        gameObject.SetActive(true);
        SetGunHolderPosition();
            //raycastWeaponItem = Item as RaycastWeaponItem;
    }

    void SetGunHolderPosition() {
        itemHolder.transform.position = Vector3.zero;
        itemHolder.transform.localPosition = Item.equipPosition;
    }

    public void StartUnEquipping() {
        Deactivate();
        medicalState = MedicalState.UnEquipping;
        gameObject.SetActive(false);
    }

    public virtual void StartReloading() {
        reloadTimer = 0;
        medicalState = MedicalState.Reloading;
    }

    public virtual void StopReloading() {
        Item.stackAmount--;
        medicalState = MedicalState.Idle;
    }
}
