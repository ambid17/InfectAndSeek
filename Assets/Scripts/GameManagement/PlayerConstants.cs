﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerConstants {
    public static float interactRange = 4;
    public static float interactTime = 0.2f;
    public static float stanceChangeTime = 0.5f;

    public const int defaultLayer = 0;
    public const int objectLayer = 12;
    public const int interactingObjectLayer = 13;
    public const int dontDrawLayer = 14;
    public const int equipmentLayer = 15;

    public static float proneColliderHeight = 0.5f;
    public static float crouchColliderHeight = 1f;
    public static float standingColliderHeight = 1.8f;

    public static Vector3 proneColliderCenter = new Vector3(0, 1.55f, 0);
    public static Vector3 crouchColliderCenter = new Vector3(0, 1.3f, 0);
    public static Vector3 standingColliderCenter = new Vector3(0, 0.9f, 0);

    public static float proneWalkSpeed = 1;
    public static float crouchWalkSpeed = 3;
    public static float standWalkSpeed = 5;
    public static float proneRunSpeed = 1.5f;
    public static float crouchRunSpeed = 4;
    public static float standRunSpeed = 8;

    public static int humanStartingHealth = 100;
    public static int infectedStartingHealth = 100;

    public static float maxObjectVelocity = 10;

    public const string PLAYER_ID_PREFIX = "player";

    public const float pronePushPower = 1f;
    public const float crouchPushPower = 1.5f;
    public const float standingPushPower = 2f;

    public static void SetLayerRecursively(GameObject gameObject, int layer)
    {
        gameObject.layer = layer;

        foreach (Transform child in gameObject.transform)
        {
            child.gameObject.layer = PlayerConstants.objectLayer;
        }
    }
}
