﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public MatchSettings matchSettings;

    private void Awake() {
        if (instance != null) {
            Debug.LogError("More than 1 game manager in scene");
        } else {
            instance = this;
        }
    }

    #region Player
    [SerializeField]
    private static Dictionary<string, Player> players = new Dictionary<string, Player>();

    public static void RegisterPlayer(string _netID, Player _player) {
        Debug.Log("player registered " + _netID);
        string _playerID = PlayerConstants.PLAYER_ID_PREFIX + _netID;
        players.Add(_playerID, _player);
        _player.transform.name = _playerID;
    }

    public static void UnRegisterPlayer(string _playerID) {
        players.Remove(_playerID);
    }

    //private void OnGUI() {
    //    GUILayout.BeginArea(new Rect(0, 0, 200, 500));
    //    GUILayout.BeginVertical();

    //    foreach (string _playerID in players.Keys) {
    //        GUILayout.Label(_playerID + " - " + players[_playerID].transform.name);
    //    }

    //    GUILayout.EndVertical();
    //    GUILayout.EndArea();
    //}

    public static Player GetPlayer(string _playerID) {
        Debug.Log("getting player " + _playerID);
        return players[_playerID];
    }
    #endregion


    int readyTimer = 5; //1 minute
    int infectingTimer = 5; //5 minutes
    int runningTimer = 150; //30 minutes
    int endingTimer = 30; //30 seconds
    
    public ServerState serverState;

    public Text countdownText;
    public Text countdownTimeText;

    public enum ServerState
    {
        ready, infecting, running, ending
    }
	void Start () {
        //StartServer();
	}
	
	void Update () {
		
	}

    

    void StartServer()
    {
        StartCoroutine(Run());
    }

    IEnumerator Run()
    {
        while (true)
        {
            ReadyPhase();
            StartCoroutine(CountdownTimer(readyTimer));
            yield return new WaitForSeconds(readyTimer);

            InfectingPhase();
            StartCoroutine(CountdownTimer(infectingTimer));
            yield return new WaitForSeconds(infectingTimer);

            RunningPhase();
            StartCoroutine(CountdownTimer(runningTimer));
            yield return new WaitForSeconds(runningTimer);

            EndingPhase();
            StartCoroutine(CountdownTimer(endingTimer));
            yield return new WaitForSeconds(endingTimer);
        }
    }

    void ReadyPhase()
    {
        ChangeState(ServerState.ready);
    }

    void InfectingPhase()
    {
        ChangeState(ServerState.infecting);
        InfectRandomPlayer();
    }

    void RunningPhase()
    {
        ChangeState(ServerState.running);
        //InvokeRepeating("CheckGameEnd", 1, 0.25f);
    }

    void CheckGameEnd()
    {
        //if (CheckForHumans())
        //{
        //    ChangeState(ServerState.ending);
        //}
    }

    //bool CheckForHumans()
    //{
        //foreach (GameObject player in players)
        //{
        //    HealthComponent healthComponent = player.GetComponent<HealthComponent>();
        //    if (healthComponent.teamType == TeamType.Human)
        //    {
        //        return true;
        //    }
        //}
        //return false;
    //}

    void EndingPhase()
    {
        ChangeState(ServerState.ending);
    }

    void ChangeState(ServerState state)
    {
        serverState = state;
    }

    void InfectRandomPlayer()
    {
        //System.Random random = new System.Random();
        //int index = random.Next(0, players.Count);
        //players[index].GetComponent<Player>().StartInfecting();

        //players[0].GetComponent<Player>().StartInfecting();
    }

    IEnumerator CountdownTimer(int time)
    {
        int currentCountdown = time;
        while (currentCountdown > 0)
        {
            if (serverState == ServerState.ready)
            {
                countdownText.text = "Waiting";
            }
            else if (serverState == ServerState.infecting)
            {
                countdownText.text = "Infecting";
            }
            else if (serverState == ServerState.running)
            {
                countdownText.text = "Survive";
            }else if(serverState == ServerState.ending)
            {
                countdownText.text = "Ending";
            }

            countdownTimeText.text = GetTime(currentCountdown);
            yield return new WaitForSeconds(1);
            currentCountdown--;
        }
    }

    string GetTime(int currentCooldown)
    {
        string toReturn;

        toReturn = currentCooldown / 60 + ":";

        if (currentCooldown % 60 < 10)
        {
            toReturn += "0" + currentCooldown % 60;
        }
        else if (currentCooldown % 60 == 0)
        {
            toReturn += "00";
        }
        else
        {
            toReturn += currentCooldown % 60;
        }

        return toReturn;
    }
}
