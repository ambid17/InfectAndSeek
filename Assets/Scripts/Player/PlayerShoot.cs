﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Networking;

//public class PlayerShoot : NetworkBehaviour {
//    private const string PLAYER_TAG = "Player";

//    [SerializeField]
//    private Camera cam;

//    [SerializeField]
//    private LayerMask layerMask;

//	void Start () {
//		if(cam == null) {
//            Debug.LogError("PlayerShoot: No camera referenced");
//            this.enabled = false;
//        }
//	}
	
//	void Update () {
//        if (PauseMenu.IsOn) {
//            if (Cursor.lockState != CursorLockMode.None) {
//                Cursor.lockState = CursorLockMode.None;
//            }
//            return;
//        }


//        if (Cursor.lockState != CursorLockMode.Locked) {
//            Cursor.lockState = CursorLockMode.Locked;
//        }

//        if (Input.GetButtonDown("Fire1")) {
//            Shoot();
//        }
//	}

//    [Client]
//    void Shoot() {
//        RaycastHit _hit;
//        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out _hit, PlayerConstants.interactRange, layerMask)) {
//            if (_hit.collider.tag == PLAYER_TAG) {
//                CmdPlayerShot(transform.name, _hit.collider.name);
//            }
//        }
//    }

//    [Command]
//    void CmdPlayerShot(string _shootingPlayerID, string _playerID) {
//        Debug.Log(_playerID + " shot by " + _shootingPlayerID);
//        Player _player = GameManager.GetPlayer(_playerID);
//        //TODO update damage based off weapon
//        Player _shootingPlayer = GameManager.GetPlayer(_shootingPlayerID);
//        _player.GetComponent<HealthComponent>().RpcTakeDamage(new DamageContext(_shootingPlayer.gameObject, 100));
//    }
//}
