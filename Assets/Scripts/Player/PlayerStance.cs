﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerStance : MonoBehaviour {
    PlayerMovement playerMovement;
    CharacterController characterController;

    float stanceTimer = 0;

    PlayerStances stance = PlayerStances.standing;

    public PlayerStances Stance {
        get {
            return stance;
        }
        set {
            stance = value;
            if (Stance == PlayerStances.prone) {
                //TODO: set camera height
                playerMovement.moveSpeed = PlayerConstants.proneWalkSpeed;
                characterController.height = PlayerConstants.proneColliderHeight;
            } else if (Stance == PlayerStances.crouched) {
                playerMovement.moveSpeed = PlayerConstants.crouchWalkSpeed;
                characterController.height = PlayerConstants.crouchColliderHeight;
            } else if (Stance == PlayerStances.standing) {
                playerMovement.moveSpeed = PlayerConstants.standWalkSpeed;
                characterController.height = PlayerConstants.standingColliderHeight;
            }
        }
    }


    public enum PlayerStances {
        prone, crouched, standing
    }

    private void Awake() {
        playerMovement = gameObject.GetComponent<PlayerMovement>();

        if (playerMovement == null)
        {
            Debug.LogError("PlayerStance.Awake() PlayerMovement not found");
        }
        characterController = gameObject.GetComponent<CharacterController>();

        if (playerMovement == null)
        {
            Debug.LogError("PlayerStance.Awake() CharacterController not found");
        }
    }

    private void FixedUpdate() {
        if (PauseMenu.IsOn) {
            if (Cursor.lockState != CursorLockMode.None) {
                Cursor.lockState = CursorLockMode.None;
            }
            return;
        }


        if (Cursor.lockState != CursorLockMode.Locked) {
            Cursor.lockState = CursorLockMode.Locked;
        }

        CheckStance();
    }

    private void Update() {
        if (PauseMenu.IsOn) {
            if (Cursor.lockState != CursorLockMode.None) {
                Cursor.lockState = CursorLockMode.None;
            }
            return;
        }


        if (Cursor.lockState != CursorLockMode.Locked) {
            Cursor.lockState = CursorLockMode.Locked;
        }
        stanceTimer += Time.deltaTime;
    }

    private void CheckStance() {
        if (stanceTimer >= PlayerConstants.stanceChangeTime) {
            if (Input.GetButtonDown("Crouch")) {
                ChangeStance(PlayerStances.crouched);
            }

            if (Input.GetButtonDown("Prone")) {
                ChangeStance(PlayerStances.prone);
            }
        }
    }

    private void ChangeStance(PlayerStances stanceChange) {
        stanceTimer = 0;
        switch (stanceChange) {
            case PlayerStances.prone:
                if (stance != PlayerStances.prone) {
                    Stance = PlayerStances.prone;
                } else {
                    //TODO: make playerMovement jump to avoid falling through floors
                    Stance = PlayerStances.standing;
                }
                break;
            case PlayerStances.crouched:
                if (stance != PlayerStances.crouched) {
                    Stance = PlayerStances.crouched;
                } else if (stance == PlayerStances.crouched) {
                    Stance = PlayerStances.standing;
                }
                break;
            case PlayerStances.standing:
                if (stance != PlayerStances.standing) {
                    Stance = PlayerStances.standing;
                }
                break;
        }
    }
}
