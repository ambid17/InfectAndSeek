﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public enum LootType
{
    MeleeWeapon, PrimaryWeapon, SecondaryWeapon, Utility, Medical
}

public class PlayerInventory : MonoBehaviour {
    public int equippedIndex;
    
    //TODO: add hotbar vs inventory
    public GameObject[] equipment;
    public Transform gunHolder;

    public float equipTimer = 0;
    public float equipTime = 0.5f;

    void Start () {
		if(gunHolder == null)
        {
            gunHolder = transform.Find("GunHolder");
        }

        equipment = new GameObject[8];
        equippedIndex = 0;
	}

    private void Update()
    {
        equipTimer += Time.deltaTime;

        CheckInventoryUpdates();
    }

    void FixedUpdate () {
        if (Input.GetButton("Fire1"))
        {
            //TODO: check to make sure we aren't carrying an object
            if(equipment[equippedIndex] != null)
            {
                equipment[equippedIndex].GetComponent<Equipment>().Activate();
            }
        }
    }

    void CheckInventoryUpdates()
    {
        if (equipTimer > equipTime)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                NextEquipment();
                equipTimer = 0;
            }

            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                PreviousEquipment();
                equipTimer = 0;
            }

            if (Input.GetButton("Drop") && !equipmentIsEmpty())
            {
                equipment[equippedIndex].GetComponent<Equipment>().Drop();
                equipment[equippedIndex] = null;
                equipTimer = 0;
                PreviousEquipment();
            }
        }
    }

    public void AddToInventory(GameObject newItem, LootType lootType)
    {
        switch (lootType)
        {
            case LootType.MeleeWeapon:
                AddMeleeWeapon(newItem);
                break;
            case LootType.SecondaryWeapon:
                AddSecondaryWeapon(newItem);
                break;
            case LootType.PrimaryWeapon:
                AddPrimaryWeapon(newItem);
                break;
            case LootType.Utility:
                AddUtility(newItem);
                break;
            case LootType.Medical:
                AddMedical(newItem);
                break;
        }
    }

    void AddMeleeWeapon(GameObject newItem)
    {
        if (equipment[0] != null)
        {
            equipment[0].GetComponent<Equipment>().Drop();
        }

        newItem.GetComponent<Equipment>().AddToInventory();
        equipment[0] = newItem;
        if (equipmentIsEmptyExcept(0))
        {
            equippedIndex = 0;
            equipment[0].GetComponent<Equipment>().StartEquipping();
        }
    }

    void AddSecondaryWeapon(GameObject newItem)
    {
        if (equipment[1] != null)
        {
            equipment[1].GetComponent<Equipment>().Drop();
        }

        newItem.GetComponent<Equipment>().AddToInventory();
        equipment[1] = newItem;
        if (equipmentIsEmptyExcept(1))
        {
            equippedIndex = 1;
            equipment[1].GetComponent<Equipment>().StartEquipping();
        }
    }

    void AddPrimaryWeapon(GameObject newItem)
    {
        if(equipment[2] == null)
        {
            newItem.GetComponent<Equipment>().AddToInventory();
            equipment[2] = newItem;
            if (equipmentIsEmptyExcept(2))
            {
                equippedIndex = 2;
                equipment[2].GetComponent<Equipment>().StartEquipping();
            }
        }
        else if (equipment[3] == null)
        {
            newItem.GetComponent<Equipment>().AddToInventory();
            equipment[3] = newItem;
            if (equipmentIsEmptyExcept(3))
            {
                equippedIndex = 3;
                equipment[3].GetComponent<Equipment>().StartEquipping();
            }
        }
        else
        {
            equipment[3].GetComponent<Equipment>().Drop();
            newItem.GetComponent<Equipment>().AddToInventory();
            equipment[3] = newItem;
            if (equipmentIsEmptyExcept(3))
            {
                equippedIndex = 3;
                equipment[3].GetComponent<Equipment>().StartEquipping();
            }
        }
    }

    void AddUtility(GameObject newItem)
    {
        if (equipment[4] == null)
        {
            newItem.GetComponent<Equipment>().AddToInventory();
            equipment[4] = newItem;
            if (equipmentIsEmptyExcept(4))
            {
                equippedIndex = 4;
                equipment[4].GetComponent<Equipment>().StartEquipping();
            }
        }
        else if (equipment[5] == null)
        {
            newItem.GetComponent<Equipment>().AddToInventory();
            equipment[5] = newItem;
            if (equipmentIsEmptyExcept(5))
            {
                equippedIndex = 5;
                equipment[5].GetComponent<Equipment>().StartEquipping();
            }
        }
        else if (equipment[6] == null)
        {
            newItem.GetComponent<Equipment>().AddToInventory();
            equipment[6] = newItem;
            if (equipmentIsEmptyExcept(6))
            {
                equippedIndex = 6;
                equipment[6].GetComponent<Equipment>().StartEquipping();
            }
        }
        else
        {
            equipment[6].GetComponent<Equipment>().Drop();
            newItem.GetComponent<Equipment>().AddToInventory();
            equipment[6] = newItem;
            if (equipmentIsEmptyExcept(6))
            {
                equippedIndex = 6;
                equipment[6].GetComponent<Equipment>().StartEquipping();
            }
        }
    }

    void AddMedical(GameObject newItem)
    {
        if (equipment[7] != null)
        {
            equipment[7].GetComponent<Equipment>().Drop();
        }

        newItem.GetComponent<Equipment>().AddToInventory();
        equipment[7] = newItem;
        if (equipmentIsEmptyExcept(7))
        {
            equippedIndex = 7;
            equipment[7].GetComponent<Equipment>().StartEquipping();
        }
    }

    void NextEquipment()
    {
        if (equipmentIsEmpty())
        {
            return;
        }

        if(equipment[equippedIndex] != null)
        {
            equipment[equippedIndex].GetComponent<Equipment>().StartUnEquipping();
        }
        
        int currentIndex;

        if(equippedIndex == equipment.Length - 1)
        {
            currentIndex = 0;
        }
        else
        {
            currentIndex = equippedIndex + 1;
        }
        
        while (equipment[currentIndex] == null)
        {
            if(currentIndex == equipment.Length - 1)
            {
                currentIndex = 0;
            }
            currentIndex++;
        }
        equippedIndex = currentIndex;
        equipment[equippedIndex].GetComponent<Equipment>().StartEquipping();
    }

    void PreviousEquipment()
    {
        if (equipmentIsEmpty())
        {
            return;
        }

        if (equipment[equippedIndex] != null)
        {
            equipment[equippedIndex].GetComponent<Equipment>().StartUnEquipping();
        }
        
        int currentIndex;

        if (equippedIndex == 0)
        {
            currentIndex = equipment.Length - 1;
        }
        else
        {
            currentIndex = equippedIndex - 1;
        }

        while (equipment[currentIndex] == null)
        {
            if (currentIndex == 0)
            {
                currentIndex = equipment.Length - 1;
            }
            currentIndex--;
        }
        equippedIndex = currentIndex;
        equipment[equippedIndex].GetComponent<Equipment>().StartEquipping();
    }

    bool equipmentIsEmpty()
    {
        for (int i = 0; i < equipment.Length; i++)
        {
            if(equipment[i] != null)
            {
                return false;
            }
        }

        return true;
    }

    bool equipmentIsEmptyExcept(int exception)
    {
        for (int i = 0; i < equipment.Length; i++)
        {
            if(i == exception)
            {
                continue;
            }
            if (equipment[i] != null)
            {
                return false;
            }
        }

        return true;
    }
}
