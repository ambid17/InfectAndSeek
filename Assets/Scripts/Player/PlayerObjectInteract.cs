﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerObjectInteract : MonoBehaviour {
    public HandController handController;
    float interactTimer = 0;

    public PlayerInventory inventory;

    public Image crosshairImage;

    private void Awake() {
        handController = gameObject.GetComponentInChildren<HandController>();
        inventory = gameObject.GetComponent<PlayerInventory>();
    }

    private void Start() {
        handController.Initialize(inventory);
    }

    private void Update() {
        if (PauseMenu.IsOn) {
            if (Cursor.lockState != CursorLockMode.None) {
                Cursor.lockState = CursorLockMode.None;
            }
            return;
        }


        if (Cursor.lockState != CursorLockMode.Locked) {
            Cursor.lockState = CursorLockMode.Locked;
        }

        interactTimer += Time.deltaTime;

        CheckCanInteract();

        if (Input.GetButton("Interact") && interactTimer >= PlayerConstants.interactTime) {
            handController.isInteracting = !handController.isInteracting;
            interactTimer = 0;
        }

        if (handController.isInteracting) {
            var scroll = Input.GetAxis("Mouse ScrollWheel");
            if (scroll > 0) {
                handController.IncreasePitch(true);
            }

            if (scroll < 0) {
                handController.IncreasePitch(false);
            }

            if (Input.GetMouseButtonDown(0)) {
                handController.IncreaseYaw(true);
            }

            if (Input.GetMouseButtonDown(1)) {
                handController.IncreaseYaw(false);
            }
        }
    }

    void CheckCanInteract() {
        //if (handController.CanInteract())
        //{
        //    crosshairImage.color = Color.green;
        //}
        //else
        //{
        //    crosshairImage.color = Color.white;
        //}
    }
}
