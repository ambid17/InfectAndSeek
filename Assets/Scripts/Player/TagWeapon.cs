﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagWeapon : MonoBehaviour {
    Camera playerCamera;
    Ray shootRay;
    protected RaycastHit shootHit;

    void Start () {
        playerCamera = GetComponentInChildren<Camera>();
    }
	
	void Update () {
        if (Input.GetButtonDown("fire1")) {
            Tag();
        }
    }

    void Tag() {
        shootRay.origin = playerCamera.transform.position;
        shootRay.direction = playerCamera.transform.forward;

        Debug.DrawRay(shootRay.origin, shootRay.direction, Color.blue, 10);
        //TODO change to remote player layer
        if (Physics.Raycast(shootRay, out shootHit, PlayerConstants.interactRange, PlayerConstants.defaultLayer)) {
            HealthComponent hit = shootHit.collider.GetComponent<HealthComponent>();
            if (hit != null) {
                DamageContext context = new DamageContext(gameObject, 100);
                Debug.Log("object hit: " + hit.name);

                hit.RpcTakeDamage(context);
            }
        }
    }
}
