﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(HealthComponent))]
public class InteractableObject : MonoBehaviour {
    Vector3 newPosition;
    Rigidbody rigidbody;
    public bool isControlled = false;

    void Start () {
        rigidbody = GetComponent<Rigidbody>();

        PlayerConstants.SetLayerRecursively(gameObject, PlayerConstants.objectLayer);

        newPosition = gameObject.transform.position;
	}

    

    private void Update() {
        if (isControlled)
        {
            rigidbody.MovePosition(newPosition);

        }
    }

    public void UpdatePosition(Vector3 newPosition) {
        this.newPosition = newPosition;
    }
}
