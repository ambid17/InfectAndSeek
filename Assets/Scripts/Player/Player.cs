﻿using UnityEngine;
using System;
using UnityStandardAssets.Characters.FirstPerson;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking;

[Serializable]
public class Player : PlayerBehavior, IRespondsToDeath {
    [Header("Player info")]
    // These strings are to be used to construct a player's name
    // by randomly combining 2 strings
    private string[] nameParts = new string[] {
                "crazy", "cat", "dog", "homie", "bobble", "mr",
                "ms", "mrs", "castle", "flip", "flop" };

    public string Name { get; private set; }

    [SerializeField]
    int score;

    public GameObject humanPrefab;
    public GameObject infectedPrefab;

    public HealthComponent health;

    public Component[] componentsToDestroy;

    enum PlayerState {
        home, inGame
    }

    // NetworkStart() is **automatically** called, when a networkObject 
    // has been fully setup on the network and ready/finalized on the network!
    // In simpler words, think of it like Unity's Start() but for the network ;)
    protected override void NetworkStart()
    {
        base.NetworkStart();

        // If this networkObject is actually the **enemy** Player
        // hence not the one we will control and own
        if (!networkObject.IsOwner)
        {
            // Don't render through a camera that is not ours
            // Don't listen to audio through a listener that is not ours
            GetComponentInChildren<Camera>().gameObject.SetActive(false);
            
            // Don't accept inputs from objects that are not ours
            GetComponent<PlayerMovement>().enabled = false;


            foreach(Component component in componentsToDestroy)
            {
                Destroy(component);
            }
            //only need 1 Camera
           
        }

        // Assign the name when this object is setup on the network
        ChangeName();
    }

    public void ChangeName()
    {
        // Only the owning client of this object can assign the name
        if (!networkObject.IsOwner)
            return;

        // Get a random index for the first name
        int first = UnityEngine.Random.Range(0, nameParts.Length - 1);
        // Get a random index for the last name
        int last = UnityEngine.Random.Range(0, nameParts.Length - 1);

        // Assign the name to the random selection
        Name = nameParts[first] + " " + nameParts[last];

        // Send an RPC to let everyone know what the name is for this player
        // We use "AllBuffered" so that if people come late they will get the
        // latest name for this object
        // We pass in "Name" for the args because we have 1 argument that 
        // is to be a string as it is set in the NCW
        networkObject.SendRpc(RPC_UPDATE_NAME, Receivers.AllBuffered, Name);
    }

    // Default Unity update method
    private void Update()
    {
        // Check to see if we are NOT the owner of this player
        if (!networkObject.IsOwner)
        {
            transform.position = networkObject.position;
            transform.rotation = networkObject.rotation;
            return;
        }
        networkObject.position = transform.position;
        networkObject.rotation = transform.rotation;
    }

    // Override the abstract RPC method that we made in the NCW
    public override void UpdateName(RpcArgs args)
    {
        // Since there is only 1 argument and it is a string we can safely
        // cast the first argument to a string, knowing that it is going to
        // be the name for this player
        Name = args.GetNext<string>();
    }






    //private void Awake() {
    //    health = gameObject.GetComponent<HealthComponent>();
    //    health.RegisterDeathResponder(this);
    //    SetHuman(true);
    //}

    //public void Set(string playerName, TeamType teamType, Vector3 position) {
    //    this.playerName = playerName;
    //    health.RegisterTeam(teamType);
    //    score = 0;

    //    transform.position = position;

    //    SetLayerRecursively(gameObject, PlayerConstants.defaultLayer);

    //    SetHuman(teamType == TeamType.Human ? true : false);
    //}

    //void SetLayerRecursively(GameObject go, int layer) {
    //    go.layer = layer;

    //    foreach (Transform child in go.transform) {
    //        SetLayerRecursively(child.gameObject, layer);
    //    }
    //}

    //public void SetHuman(bool isHuman) {
    //    humanPrefab.SetActive(isHuman);
    //    infectedPrefab.SetActive(!isHuman);

    //    if (isHuman) {
    //        health.maxHealth = 100;
    //        health.RegisterTeam(TeamType.Human);
    //    } else {
    //        health.maxHealth = 1000;
    //        health.RegisterTeam(TeamType.Infected);
    //    }
    //}

    //public void StartInfecting() {
    //    SetHuman(false);
    //}

    public TeamType GetPlayerTeam()
    {
        return health.teamType;
    }

    public void OnDeath(DamageContext context)
    {
        Debug.Log("player: " + Name + " died");
        RespawnAsInfected();
    }

    void RespawnAsInfected() {
        //TODO add respawn options: respawn in 30 seconds at current location
        //OR respawn somewhere random
        //if human change team
    }
}