﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerAnimation : MonoBehaviour {
    Animator anim;
    PlayerStance playerStance;
    FirstPersonController fpsController;

	void Start () {
        anim = GetComponent<Animator>();
        playerStance = GetComponent<PlayerStance>();
        fpsController = GetComponent<FirstPersonController>();
    }

    void Update() {
        if (PauseMenu.IsOn) {
            if (Cursor.lockState != CursorLockMode.None) {
                Cursor.lockState = CursorLockMode.None;
            }
            return;
        }


        if (Cursor.lockState != CursorLockMode.Locked) {
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (fpsController.m_Input != Vector2.zero) {
            if (!fpsController.m_IsWalking) {
                anim.SetBool("isRunning", true);
                anim.SetBool("isWalking", false);
            } else {
                anim.SetBool("isWalking", true);
                anim.SetBool("isRunning", false);
            }
        } else {
            anim.SetBool("isWalking", false);
            anim.SetBool("isRunning", false);

        }
    }
}
