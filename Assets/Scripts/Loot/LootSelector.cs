﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootSelector: MonoBehaviour {
    public InventoryItem[] meleeLoot;
    public InventoryItem[] secondaryLoot;
    public InventoryItem[] primaryLoot;
    public InventoryItem[] utilityLoot;
    public InventoryItem[] medicalLoot;

    static System.Random random;

    private void Awake()
    {
        random = new System.Random();
    }

    public InventoryItem GetRandomLootOfType(LootType lootType)
    {
        switch (lootType)
        {
            case LootType.MeleeWeapon:
                return GetRandomLootFrom(meleeLoot);
            case LootType.SecondaryWeapon:
                return GetRandomLootFrom(secondaryLoot);
            case LootType.PrimaryWeapon:
                return GetRandomLootFrom(primaryLoot);
            case LootType.Utility:
                return GetRandomLootFrom(utilityLoot);
            case LootType.Medical:
                return GetRandomLootFrom(medicalLoot);
        }

        return null;
    }

    public InventoryItem GetRandomLootFrom(InventoryItem[] lootArray)
    {
        int randomIndex = random.Next(lootArray.Length);
        return lootArray[randomIndex];
    }
}
