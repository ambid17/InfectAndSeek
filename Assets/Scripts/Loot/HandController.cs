﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HandController : MonoBehaviour
{
    public InteractableObject pickedObject;
    public HingeJoint joint;
    public Camera playerCamera;

    PlayerInventory inventory;
    public bool isInteracting = false;

    LayerMask objectLayerMask = 1 << PlayerConstants.objectLayer;

    bool hasObject = false;


    public void Initialize(PlayerInventory inventory)
    {
        this.inventory = inventory;
    }

    private void Start()
    {
        playerCamera = gameObject.GetComponentInParent<Camera>();
    }

    public void Update()
    {
        gameObject.transform.position = playerCamera.transform.position + (playerCamera.transform.forward * PlayerConstants.interactRange);
        
        if (isInteracting)
        {
            if (pickedObject == null)
            {
                PickObject();
            }
        } else
        {
            if(pickedObject != null)
            {
                ResetPickedObject();
            }
        }

        //TODO: add something about tick rate
        if (hasObject) {
            pickedObject.UpdatePosition(gameObject.transform.position);
        }
    }

    void PickObject()
    {
        RaycastHit hit;
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);

        if(Physics.Raycast(ray, out hit, PlayerConstants.interactRange, objectLayerMask)){
            LootItem item;
            if (item = hit.collider.GetComponent<LootItem>())
            {
                isInteracting = false;
                item.EquipToPlayer(inventory);
            }else if (hit.collider.GetComponentInParent<InteractableObject>())
            {
                GameObject hitObject = hit.collider.gameObject;
                pickedObject = hitObject.GetComponentInParent<InteractableObject>();
                PlayerConstants.SetLayerRecursively(hitObject, PlayerConstants.interactingObjectLayer);
                pickedObject.isControlled = true;
                Rigidbody rb = pickedObject.GetComponent<Rigidbody>();
                rb.constraints = RigidbodyConstraints.FreezeRotation;
                rb.useGravity = false;

                hasObject = true;
            }
        }
        else
        {
            isInteracting = false;
        }
    }

    void ResetPickedObject() {
        pickedObject.isControlled = false;
        Rigidbody rb = pickedObject.GetComponent<Rigidbody>();
        rb.useGravity = true;
        rb.constraints = RigidbodyConstraints.None;
        PlayerConstants.SetLayerRecursively(pickedObject.gameObject, PlayerConstants.objectLayer);
        pickedObject = null;
        hasObject = false;
    }

    public bool CanInteract()
    {
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, PlayerConstants.interactRange, objectLayerMask))
        {
            if (hit.collider.GetComponent<LootItem>() || hit.collider.GetComponentInParent<InteractableObject>())
            {
                Debug.Log("CanInteract with: " + hit.collider.gameObject.name);
                return true;
            }
        }

        return false;
    }

    public void IncreasePitch(bool isIncrease) {
        if (pickedObject != null) {
            if (isIncrease) {
                pickedObject.transform.Rotate(0, 10, 0);
            } else {
                pickedObject.transform.Rotate(0, -10, 0);
            }
        }
    }

    public void IncreaseYaw(bool isIncrease) {
        if (pickedObject != null) {
            if (isIncrease) {
                pickedObject.transform.Rotate(0, 0, 10);
            } else {
                pickedObject.transform.Rotate(0, 0, -10);
            }
        }
    }
}
