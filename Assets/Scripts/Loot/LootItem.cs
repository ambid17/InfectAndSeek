﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootItem : MonoBehaviour {
    public InventoryItem item;

    private void Start()
    {
        PlayerConstants.SetLayerRecursively(gameObject, PlayerConstants.objectLayer);
        if (!gameObject.GetComponent<BoxCollider>())
        {
            gameObject.AddComponent<BoxCollider>();
        }
        if (!gameObject.GetComponent<Rigidbody>())
        {
            Rigidbody rb = gameObject.AddComponent<Rigidbody>();
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        }
    }

    public void EquipToPlayer(PlayerInventory inventory)
    {
        if(item is RaycastWeaponItem)
        {
            RaycastWeapon weapon = gameObject.AddComponent<RaycastWeapon>();
            weapon.Item = item as WeaponItem;
        } else if (item is ProjectileWeaponItem) {
            ProjectileWeapon weapon = gameObject.AddComponent<ProjectileWeapon>();
            weapon.Item = item as ProjectileWeaponItem;
        } else if(item is MeleeWeaponItem) {
            MeleeWeapon weapon = gameObject.AddComponent<MeleeWeapon>();
            weapon.Item = item as WeaponItem;
        } else if(item is MedicalItem) {
            Medical medical = gameObject.AddComponent<Medical>();
            medical.Item = item as MedicalItem;
        } else if(item is GrenadeItem) {
            GrenadeProjectileWeapon weapon = gameObject.AddComponent<GrenadeProjectileWeapon>();
            weapon.Item = item as GrenadeItem;
        }

        gameObject.transform.parent = inventory.gunHolder;
        PlayerConstants.SetLayerRecursively(gameObject, PlayerConstants.equipmentLayer);
        inventory.AddToInventory(gameObject, item.lootType);
        GameObject.Destroy(GetComponent<Collider>());
        GameObject.Destroy(GetComponent<Rigidbody>());
        GameObject.Destroy(this);
    }
}
