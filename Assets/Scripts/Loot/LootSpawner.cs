﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootSpawner : MonoBehaviour {
    InventoryItem lootToSpawn;

    public LootType lootType;

	void Start () {
        LootSelector lootSelector = GameObject.Find("GameManager").GetComponent<LootSelector>();
        lootToSpawn = lootSelector.GetRandomLootOfType(lootType);

        GameObject loot = GameObject.Instantiate(lootToSpawn.itemPrefab);

        SetupObject(loot);

        //turn off animation, not destroy
        Animation animation = loot.GetComponent<Animation>();
        if(animation != null){
            GameObject.Destroy(animation);
        }

        LootItem item = loot.AddComponent<LootItem>();
        item.item = lootToSpawn;
	}

    void SetupObject(GameObject loot) {
        loot.transform.parent = gameObject.transform;
        loot.transform.position = Vector3.zero;
        loot.transform.localPosition = Vector3.zero;
        Rigidbody rb = loot.GetComponent<Rigidbody>();

        if(rb == null) {
            rb = loot.AddComponent<Rigidbody>();
            rb.useGravity = true;
        }

        BoxCollider col = loot.GetComponent<BoxCollider>();

        if(col == null) {
            col = loot.AddComponent<BoxCollider>();
        }
    }
}
