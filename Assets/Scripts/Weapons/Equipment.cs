﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Equipment
{
    //Start playing equip animation.
    void StartEquipping();
    //Start playing unequip animation.
    void StartUnEquipping();

    void AddToInventory();
    void Drop();

    bool CanActivate();
    void Activate();
    void Deactivate();

    void EnableEffects();
    void DisableEffects();
}