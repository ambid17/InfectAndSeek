﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour {
    protected float m_FadeSpeed = 0.075f;               // amount of alpha to be deducted each frame
    protected bool m_ForceShow = false;                 // used to set the muzzleflash 'always on' in the editor
    protected Color m_Color = new Color(1, 1, 1, 0.0f);

    protected Transform m_Transform = null;

    public float FadeSpeed { get { return m_FadeSpeed; } set { m_FadeSpeed = value; } }
    public bool ForceShow { get { return m_ForceShow; } set { m_ForceShow = value; } }

    protected Light m_Light = null;
    protected float m_LightIntensity = 0.0f;

    protected Renderer m_Renderer = null;
    protected Material m_Material = null;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Activate() {

    }
}
