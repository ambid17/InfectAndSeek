﻿using UnityEngine;
using System.Collections;

public class ProjectileShootTriggerable : MonoBehaviour
{
    [HideInInspector] public Rigidbody projectile;
    public Vector3 bulletSpawn;
    [HideInInspector] public float projectileForce = 250f;

    public void Activate()
    {
        //Instantiate a copy of our projectile and store it in a new rigidbody variable called clonedBullet
        Rigidbody clonedBullet = Instantiate(projectile, bulletSpawn, transform.rotation) as Rigidbody;

        //Add force to the instantiated bullet, pushing it forward away from the bulletSpawn location, using projectile force for how hard to push it away
        clonedBullet.AddForce(bulletSpawn * projectileForce);
    }
}
