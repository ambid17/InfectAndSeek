﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeProjectileWeapon : ProjectileWeapon {
    public GrenadeItem grenadeItem;
    float cookTimer = 0;
    bool isCooking = false;
    public override void FixedUpdate() {
        if (isCooking) {
            cookTimer += Time.deltaTime;
        }
        if(cookTimer > grenadeItem.timeToExplode) {
            ExplodeGrenade();
        }

        if (Input.GetButtonDown("Fire1")) {
            isCooking = true;
        }
        if (Input.GetButtonUp("Fire1")) {
            isCooking = false;
            Activate();
        }
    }

    public override void Activate() {
        if (CanActivate()) {
            shootTimer = 0;
            weaponState = WeaponState.Firing;
            if (grenadeItem.usesAmmo)
                bulletsInMag--;

            EnableEffects();

            ThrowGrenade();
        } else if (weaponState == WeaponState.Idle) {
            StartReloading();
        }
    }

    public void ThrowGrenade() {
        cookTimer = 0;
        Vector3 origin = gameObject.transform.position;
        Quaternion rot = mainCamera.transform.rotation;
        GameObject proj = Instantiate(projectileToLaunch, origin, rot);
        GrenadeProjectile projectile = proj.GetComponent<GrenadeProjectile>();
        projectile.Initialize(grenadeItem, cookTimer);
        Rigidbody rigidBody = proj.GetComponent<Rigidbody>();
        Vector3 velocity = mainCamera.transform.TransformVector(Vector3.forward * projectileWeaponItem.launchSpeed);
        rigidBody.velocity = velocity;
    }

    public void ExplodeGrenade() {
        cookTimer = 0;
        Vector3 origin = gameObject.transform.position;
        Quaternion rot = mainCamera.transform.rotation;
        GameObject proj = Instantiate(projectileToLaunch, origin, rot);
        GrenadeProjectile projectile = proj.GetComponent<GrenadeProjectile>();
        projectile.Initialize(grenadeItem, grenadeItem.timeToExplode);
    }

    public override void StartEquipping() {
        base.StartEquipping();
        grenadeItem = Item as GrenadeItem;
        projectileToLaunch = grenadeItem.projectile;
        isCooking = false;

        cookTimer = 0;
    }

    public override void StartUnEquipping() {
        isCooking = false;
        cookTimer = 0;
        base.StartUnEquipping();
    }
}
