﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/*A weapon that performs a raytrace for hit detection */

public class RaycastWeapon : Weapon
{
    protected Ray shootRay = new Ray();
    protected RaycastHit shootHit;
    RaycastWeaponItem raycastWeaponItem;

    public override void Activate()
    {
        if (CanActivate()) {
            weaponState = WeaponState.Firing;

            if (Item.usesAmmo)
                bulletsInMag--;

            shootTimer = 0;

            EnableEffects();

            shootRay.origin = mainCamera.transform.position;
            shootRay.direction = mainCamera.transform.forward;

            //Debug.DrawRay(shootRay.origin, shootRay.direction, Color.blue, 10);
            if (Physics.Raycast(shootRay, out shootHit, raycastWeaponItem.range, 10)) {
                HealthComponent hit = shootHit.collider.GetComponent<HealthComponent>();
                if (hit != null) {
                    DamageContext context = new DamageContext(gameObject.transform.parent.gameObject, Item.damage, shootHit.point, damageType);
                    Debug.Log("object hit: " + hit.name);

                    hit.RpcTakeDamage(context);
                }
            }
        } else if(weaponState == WeaponState.Idle) {
            StartReloading();
        }
    }

    public override void Deactivate()
    {
        if(weaponState != WeaponState.Idle) {
            weaponState = WeaponState.Idle;
            DisableEffects();
        }
    }

    void Update()
    {
        shootTimer += Time.deltaTime;
        reloadTimer += Time.deltaTime;
        equipTimer += Time.deltaTime;
        if (IsReloading() && reloadTimer >= Item.timeToReload)
        {
            StopReloading();
        }
        if (shootTimer >= Item.timeBetweenShots && weaponState == WeaponState.Firing) {
            Deactivate();
        }
        if(equipTimer >= Item.timeToEquip && weaponState == WeaponState.Equipping) {
            weaponState = WeaponState.Idle;
        }
    }

    public override bool CanShoot()
    {
        //Debug.Log("RaycastWeapon.CanShoot(): state=" + weaponState + " hasBullets="+ HasBullets() +  " uses ammo=" + raycastWeaponItem.usesAmmo + " timer=" + shootTimer);
        return weaponState == WeaponState.Idle && (HasBullets() || !Item.usesAmmo) && shootTimer >= Item.timeBetweenShots;
    }

    public override void StartEquipping() {
        base.StartEquipping();
        raycastWeaponItem = Item as RaycastWeaponItem;
    }
}
