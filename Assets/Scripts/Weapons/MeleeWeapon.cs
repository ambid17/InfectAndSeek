﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : RaycastWeapon {
    MeleeWeaponItem meleeWeaponItem;

    public MeleeWeaponItem MeleeWeaponItem {
        set {
            gunParticles = GetComponentInChildren<ParticleSystem>();
            meleeWeaponItem = value;
        }
    }
}
