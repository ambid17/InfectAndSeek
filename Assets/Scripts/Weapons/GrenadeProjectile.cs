﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeProjectile : MonoBehaviour {
    GameObject explosion;
    GrenadeItem grenadeItem;

    float explosionTimer = 0;

    public void Initialize(GrenadeItem grenadeItem, float cookTime) {
        this.grenadeItem = grenadeItem;
        explosionTimer = cookTime;
    }

    private void Update() {
        explosionTimer += Time.deltaTime;
        if(explosionTimer >= grenadeItem.timeToExplode) {
            Explode();
        }
    }

    void Explode() {
        var colliders = Physics.OverlapSphere(transform.position, grenadeItem.explosionRadius);
        foreach (Collider other in colliders) {
            var hit = other.gameObject.GetComponent<HealthComponent>();
            if (hit != null) {
                DamageContext context = new DamageContext(gameObject.transform.parent.gameObject, grenadeItem.damage);
                hit.RpcTakeDamage(context);
            }

            Rigidbody rbody = other.GetComponent<Rigidbody>();
            if (rbody != null) {
                rbody.AddExplosionForce(grenadeItem.explosionPower, transform.position, grenadeItem.explosionRadius, grenadeItem.explosionLift);
            }
        }

        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public virtual void OnCollisionEnter(Collision collision) {
        //TODO play collision sound as it bounces off of objects
    }
}
