﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int damage = 20;
    public float explosionRadius = 10f;
    public float explosionPower = 10f;
    public float explosionLift = 10f;

    private GameObject creator;

    private GameObject explosion;

    public void Initialize(ProjectileWeaponItem projectileWeaponItem) {
        damage = projectileWeaponItem.damage;
        explosionRadius = projectileWeaponItem.explosionRadius;
        explosionPower = projectileWeaponItem.explosionPower;
        explosionLift = projectileWeaponItem.explosionLift;
        explosion = projectileWeaponItem.explosion;
    }

    public void SetCreator(GameObject creator)
    {
        this.creator = creator;
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        //TODO allow you to kill yourself (fixed with networking using local vs remote layer)
        if (collision.gameObject.layer == PlayerConstants.defaultLayer) return;

        var colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach(Collider other in colliders)
        {
            var hit = other.gameObject.GetComponent<HealthComponent>();
            if(hit != null) {
                if (hit.teamType != creator.GetComponent<Player>().GetPlayerTeam()) {
                    DamageContext context = new DamageContext(gameObject.transform.parent.gameObject, damage);
                    hit.RpcTakeDamage(context);
                }
            }

            Rigidbody rbody = other.GetComponent<Rigidbody>();
            if (rbody != null)
            {
                rbody.AddExplosionForce(explosionPower, transform.position, explosionRadius, explosionLift);
            }
        }

        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
