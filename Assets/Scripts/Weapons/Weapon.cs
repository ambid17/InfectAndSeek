﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponState {
    Unequipped, Equipping, UnEquipping, Firing,
    Reloading, Idle
}

public abstract class Weapon : MonoBehaviour, Equipment
{
    protected Camera mainCamera;
    public Transform gunHolder;
    //public Animator animator;
    
    public ParticleSystem gunParticles;
    public AudioSource gunAudio;
    public AudioSource reloadAudio;
    public DamageType damageType;

    /*Tracks current Weapon State*/
    WeaponState state;

    public WeaponState weaponState {
        set {
            state = value;
            Debug.Log("weaponState set: " + value);
        }
        get {
            return state;
        }
    }

    public int bulletsInMag;
    protected float shootTimer;
    protected float reloadTimer;
    protected float equipTimer;
    
    public PlayerObjectInteract player;

    protected LayerMask shootableLayerMask;

    protected WeaponItem item;

    public WeaponItem Item {
        get {
            gunParticles = GetComponentInChildren<ParticleSystem>();
            return item;
        }
        set {
            item = value;
        }
    }

    protected virtual void Start()
    {
        shootTimer = 0;
        reloadTimer = 0;
        equipTimer = 0;
        shootableLayerMask = ~(1 << (PlayerConstants.defaultLayer));
    }

    public virtual void FixedUpdate() {
        if (Input.GetButton("Fire1")) {
            Activate();
        }
    }

    public virtual bool CanShoot()
    {
        if (HasBullets() && state == WeaponState.Idle) {
            return true;
        } else {
            return false;
        }
    }

    public virtual bool CanActivate()
    {
        return CanShoot();
    }

    public virtual void StartReloading()
    {
        reloadTimer = 0;
        weaponState = WeaponState.Reloading;
        //animator.SetBool("IsReloading", true);
        reloadAudio.Play();
    }

    public virtual void StopReloading()
    {
        bulletsInMag = Item.bulletsPerMag;
        weaponState = WeaponState.Idle;
        reloadAudio.Stop();
    }

    public bool HasBullets()
    {
        return bulletsInMag > 0;
    }

    public int GetBulletsInMag()
    {
        return bulletsInMag;
    }

    public virtual void SetBulletsInMag() { }

    public bool IsReloading()
    {
        return weaponState == WeaponState.Reloading;
    }

    public virtual void EnableEffects()
    {
        if (gunAudio != null)
            gunAudio.Play();
        if (gunParticles != null)
            gunParticles.Play();
    }

    public virtual void DisableEffects()
    {
        if (gunAudio != null)
            gunAudio.Stop();
        if (gunParticles != null)
            gunParticles.Stop();
    }

    public virtual void StartEquipping()
    {
        equipTimer = 0;
        weaponState = WeaponState.Equipping;
        gameObject.SetActive(true);
        SetGunHolderPosition();
    }

    void SetGunHolderPosition() {
        gunHolder.transform.position = Vector3.zero;
        gunHolder.transform.localPosition = Item.equipPosition;
    }

    public virtual void StartUnEquipping()
    {
        Deactivate();
        weaponState = WeaponState.UnEquipping;
        gameObject.SetActive(false);
    }

    public virtual void AddToInventory() {
        if (gunHolder == null) {
            player = GetComponentInParent<PlayerObjectInteract>();
            gunHolder = player.inventory.gunHolder;
        }

        SetAudio();

        SetTransform();

        SetCamera();
        gameObject.SetActive(false);
    }

    void SetAudio() {
        if (gunAudio == null) {
            gunAudio = gameObject.AddComponent<AudioSource>();
            gunAudio.playOnAwake = false;
            gunAudio.clip = Item.shotSound;
        }

        if (reloadAudio == null) {
            reloadAudio = gameObject.AddComponent<AudioSource>();
            reloadAudio.playOnAwake = false;
            reloadAudio.clip = Item.reloadSound;
        }
    }

    void SetTransform() {

        gameObject.transform.position = Vector3.zero;
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.eulerAngles = Vector3.zero;
        gameObject.transform.localEulerAngles = Vector3.zero;
    }

    void SetCamera() {
        mainCamera = player.gameObject.GetComponentInChildren<Camera>();
    }

    public void Drop() {
        Deactivate();

        gameObject.transform.parent = null;
        gameObject.AddComponent<BoxCollider>();

        Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.useGravity = true;
        rigidbody.AddForce(gameObject.transform.position * 2);
        rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        LootItem item = gameObject.AddComponent<LootItem>();
        item.item = this.item;

        PlayerConstants.SetLayerRecursively(gameObject, PlayerConstants.objectLayer);
        gameObject.SetActive(true);

        GameObject.Destroy(this);
    }

    public abstract void Activate();
    public abstract void Deactivate();
}
