﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileWeapon : Weapon
{
    public GameObject projectileToLaunch;
    
    public ProjectileWeaponItem projectileWeaponItem;

    public override void Activate()
    {
        if (CanActivate()) {
            shootTimer = 0;
            weaponState = WeaponState.Firing;
            if (projectileWeaponItem.usesAmmo)
                bulletsInMag--;

            EnableEffects();

            ShootProjectile();
        } else if (weaponState == WeaponState.Idle) {
            StartReloading();
        }
    }

    public void ShootProjectile() {
        Vector3 origin = gameObject.transform.position;
        Quaternion rot = mainCamera.transform.rotation;
        GameObject proj = Instantiate(projectileToLaunch, origin, rot);
        Projectile projectile = proj.GetComponent<Projectile>();
        projectile.Initialize(projectileWeaponItem);
        projectile.SetCreator(transform.root.gameObject);
        Rigidbody rigidBody = proj.GetComponent<Rigidbody>();
        Vector3 velocity = mainCamera.transform.TransformVector(Vector3.forward * projectileWeaponItem.launchSpeed);
        rigidBody.velocity = velocity;
    }

    public override void Deactivate()
    {
        if (weaponState != WeaponState.Idle) {
            weaponState = WeaponState.Idle;
            DisableEffects();
        }
    }
    
    void Update()
    {
        shootTimer += Time.deltaTime;
        reloadTimer += Time.deltaTime;
        equipTimer += Time.deltaTime;
        if (IsReloading() && reloadTimer >= Item.timeToReload) {
            StopReloading();
        }
        if (shootTimer >= Item.timeBetweenShots && weaponState == WeaponState.Firing) {
            Deactivate();
        }
        if (equipTimer >= Item.timeToEquip && weaponState == WeaponState.Equipping) {
            weaponState = WeaponState.Idle;
        }
    }

    public override bool CanShoot()
    {
        //Debug.Log("state " + weaponState + " uses ammo " + projectileWeaponItem.usesAmmo + " timer " + shootTimer);
        return weaponState == WeaponState.Idle && (HasBullets() || !Item.usesAmmo) && shootTimer >= Item.timeBetweenShots;
    }

    public override void StopReloading()
    {
        bulletsInMag = projectileWeaponItem.bulletsPerMag;
        weaponState = WeaponState.Idle;
        //animator.SetBool("IsReloading", false);
        reloadAudio.Stop();
    }

    public override void StartEquipping()
    {
        base.StartEquipping();
        projectileWeaponItem = Item as ProjectileWeaponItem;
        projectileToLaunch = projectileWeaponItem.projectile;
    }
}
