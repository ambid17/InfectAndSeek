﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
	void Start () {
        StartCoroutine(Explode());
    }

    IEnumerator Explode() {
        Debug.Log("");
        var systems = GetComponentsInChildren<ParticleSystem>();
        foreach(ParticleSystem system in systems) {
            system.Play();
        }

        yield return new WaitForSeconds(3);

        Destroy(gameObject);
    }
}
