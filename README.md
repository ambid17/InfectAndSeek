# Overview
You and 10-1000 other people are in a crowded building/area and a person randomly starts to look crazy.
They are becoming infected with someting. Hold out for 30 minutes until the cure arrives.
* All numbers are placeholders
* Things marked with a "?" are in brainstorming

# Home page
## Join Lobby
You can join a private server or an official server

## Settings
Change key binds, sensitivty, audio, and video

## Loadout
* change appearance
* change clothes
* choose starting weapon


# Game Start
* one person in the lobby is randomly selected to be the infected
* hiders get 5 minutes to get away from the person becoming infected (maybe with a vehicle of some sort)
* after 5 minutes the infected has transformed and has to eat humans to stay alive


# Mid Game
## Run
* use vehicles to get away, but beware of the noise
* find weapons

## Hide
* improve your fort, be on the lookout, and stay quiet
* once you have a spot, might as well turn it into a fort!
* barricade doors with items in the building, based on the weight and size of the object the barricade will get more hp

### Barricade items
* bookshelves (50hp)
* desks (100hp)
* boxes (10hp)
* chains to wrap around the door handles (200hp)
* different types of doors (wood = 200 hp, steel = 1000hp)

## Fight
* fight the zombie with a group of people
* try to group up with slows and stuns so the melee attacks can do work
* if you can, use a ranged weapon

## Weapons
### Starting weapons
You will randomly be spawned with one of the following
* taser (acts as a temporary stun)
* tranquilizer gun (acts as a slow)
* melee weapon (axe, sword, etc... weapons with different damage, speed and strikes)

### Found Weapons
* Assault rifle
* Rocker Launcher
* Sniper rifle
* Sub Machine gun

## Play dead
* You can hope the zombie doesn't eat your opossum-like body

## Death as a human
Choose:
* Spawn in as a zombie
* Become a ghost that can move objects
* Spectate


# End Game
Try to be as sneaky as possible and have a fort ready

# Infected mechanics
* can't go through windows or climb?
* if you die, you can either wait one minute at your body, and reanimate whenever OR respawn somewhere random instantly

## Infected abilities
* Basic/melee attack (no cooldown)
* This causes damage to the human, and kills them in 3 hits?

### Infection projectile vomit (1 minute cooldown)
* If a human is hit they are instantly infected

### Super Sprint (30 second cooldown)
* Infected gains a massive speed boost for a short time

### Shoulder Bash (2 minute cooldown)
* The infected can do tons of damage to a barricade

### Sticky goop (3 minute cooldown)
* Shoots a blob of goop that slows enemies in it, lasts indefinitely

### Consume a body
* Consuming a human body resets cooldowns, gives hp regen
* Consuming a zombie body upgrades your abilities, and combines you with the player?
* The first level gives the zombie more heads, and the second-fourth players can shoot Sticky goop or projectile vomit
* More levels to be thought out


## Notes
If you join the lobby midgame, you will be randomly spawned as a zombie or human with a vehicle and full gas